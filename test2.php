<html>
<head>
    <meta charset="UTF-8">
    <title>Taban Shahr</title>
</head>
<body>

<?
function hr($return = false){
    if ($return){
        return "<hr>\n";
    } else{
        echo "<hr>\n";
    }
}

function br($return = false){
    if ($return){
        return "<br>\n";
    } else{
        echo "<br>\n";
    }
}

function dump($var,$return = false){
    if (is_array($var)){
        $out = print_r($var,true);
    } else if (is_object($var)){
        $out = var_export($var,true);
    } else{
        $out = $var;
    }
    if ($return){
        return "\n<pre>$out</pre>\n";
    } else{
        echo "\n<pre>$out</pre>\n";
    }
}

function test1($param1,$param2,$param3=0,$param4=0,$param5=0){
    return $param1+$param2+$param3+$param4+$param5;
}

function catenate($str1,$str2,$str3='',$str4=''){

    $output=$str1.' '.$str2;

    if (strlen($str3)>0){
        $output.=' '.$str3;
    }
    if (strlen($str4)>0) {
        $output .= ' ' . $str4;
    }
    return $output;
}

function division($number,$print = false){
    $output = array();
    for ($i=1; $i<=$number; $i++){
        if ($number % $i == 0){
            $output[] = $i;
            if ($print) {
                echo $i . br(true);
                //echo br(false);
                //echo br();
            }
        }
    }
    return $output;
}

function compute($a,$b){
    $sum = $a + $b;
    $minus = $a - $b;
    $multiply = $a * $b;
    $remain = $a % $b;

    return array($sum,$minus,$multiply,$remain);
}
?>

<?

require_once('sample.php');
//require_once('sample.php');
hr();
echo test1(2,12,7,5,6);
hr();
echo catenate("hello","world");
hr();
$divisions = division(1000,true);
hr();
dump($divisions);
hr();
list($plus,$minus,$multiply,$remain) = compute(10,4);
echo $plus;
hr();
echo Car::$capacity;
br();
$car = new Car();
$car->getcolor();
br();
$car->color = "blue";
$car->getcolor();
br();

?>

</body>
</html>
